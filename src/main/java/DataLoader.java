import org.openstreetmap.osm.Node;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.function.Function;
import java.util.function.Supplier;

public interface DataLoader {
    default void cleanup(Connection con) throws SQLException {
        try (Statement stmt = con.createStatement()) {
            for (String table : getTables()) {
                stmt.execute("delete from " + table);
            }
        }
    }

    String[] getTables();

    int loadData(Connection con, Function<Integer,Boolean> cond) throws IOException, JAXBException, SQLException;
}
