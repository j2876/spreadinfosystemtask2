import org.openstreetmap.osm.Node;
import org.postgresql.jdbc.PgArray;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.function.Function;
import java.util.function.Supplier;

public class NodePreparedStatementCtypeImport implements DataLoader{

    String osm_user_import;
    String node_import;
    String tag_import;
    public  NodePreparedStatementCtypeImport(String osm_user_import, String node_import, String tag_import) {
        this.osm_user_import = osm_user_import;
        this.node_import = node_import;
        this.tag_import = tag_import;
    }

    @Override
    public String[] getTables() {
        return new String[] {"tag","node","node_json","node_ctype","osm_user"};
    }

    @Override
    public int loadData(Connection con, Function<Integer,Boolean> cond) throws IOException, JAXBException {

        JAXBContext context = JAXBContext.newInstance(Node.class);
        try (var in = new FileReader("test.xml")) {
            var xmlReader = XMLInputFactory.newInstance().createXMLStreamReader(in);
            XMLStreamReader wrapper = new StreamReaderDelegate(xmlReader) {
                @Override
                public int getNamespaceCount() {
                    return 1;
                }

                @Override
                public String getNamespaceURI(int index) {
                    return "http://openstreetmap.org/osm/0.6";
                }
                @Override
                public String getNamespaceURI(String index) {
                    return "http://openstreetmap.org/osm/0.6";
                }
                @Override
                public String getNamespaceURI() {
                    return "http://openstreetmap.org/osm/0.6";
                }

                @Override
                public String getNamespacePrefix(int index) {
                    return "osm";
                }
            };
            var unmarshaller = context.createUnmarshaller();

            xmlReader.nextTag();
            xmlReader.nextTag();
            xmlReader.nextTag();

            PreparedStatement userPreparedStatement = con.prepareStatement(osm_user_import);
            PreparedStatement nodePreparedStatement = con.prepareStatement(node_import);

            HashSet<BigInteger> users = new HashSet<BigInteger>();
            var count = 0;

            while (wrapper.hasNext()) {
                if (cond.apply(count)) {
                    break;
                }
                JdbcImportMain.skipElements(wrapper);
                var element = unmarshaller.unmarshal(wrapper);
                if (!(element instanceof Node)) {
                    continue;
                }
                Node node = (Node) element;

                String userName = node.getUser();
                if (!users.contains(node.getUid())) {
                    userPreparedStatement.setBigDecimal(1, new BigDecimal(node.getUid()));
                    userPreparedStatement.setString(2, userName.replace("'", "''"));
                    userPreparedStatement.executeUpdate();
                    users.add(node.getUid());
                }
                nodePreparedStatement.setBigDecimal(1,new BigDecimal(node.getId()));
                nodePreparedStatement.setBigDecimal(2,new BigDecimal(node.getVersion()));
                nodePreparedStatement.setDate(3, new Date(node.getTimestamp().getMillisecond()));
                nodePreparedStatement.setBigDecimal(4,new BigDecimal(node.getUid()));
                nodePreparedStatement.setBigDecimal(5,new BigDecimal(node.getChangeset()));
                nodePreparedStatement.setDouble(6,node.getLat());
                nodePreparedStatement.setDouble(7,node.getLon());
                nodePreparedStatement.setArray(8, con.createArrayOf("tag_pair", node.getTag().stream().map(tag -> {return new TagFormat(tag);}).toArray()));
                nodePreparedStatement.executeUpdate();
                count++;
            }
            return count;
        } catch (XMLStreamException | SQLException | FileNotFoundException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return 0;
        }
    }
}
