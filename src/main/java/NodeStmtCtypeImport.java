import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import org.openstreetmap.osm.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Function;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import com.fasterxml.jackson.databind.jsonschema.JsonSchema;
import org.openstreetmap.osm.Tag;


public class NodeStmtCtypeImport implements DataLoader {

    String osm_user_import;
    String node_import;
    String tag_import;
    public  NodeStmtCtypeImport(String osm_user_import, String node_import, String tag_import) {
        this.osm_user_import = osm_user_import;
        this.node_import = node_import;
        this.tag_import = tag_import;
    }
    @Override
    public String[] getTables() {
        return new String[] {"tag","node","node_json","node_ctype","osm_user"};
    }

    @Override
    public int loadData(Connection con, Function<Integer,Boolean> cond) throws IOException, JAXBException {

        JAXBContext context = JAXBContext.newInstance(Node.class);
        try (var in = new FileReader("test.xml")) {
            var xmlReader = XMLInputFactory.newInstance().createXMLStreamReader(in);
            XMLStreamReader wrapper = new StreamReaderDelegate(xmlReader) {
                @Override
                public int getNamespaceCount() {
                    return 1;
                }

                @Override
                public String getNamespaceURI(int index) {
                    return "http://openstreetmap.org/osm/0.6";
                }

                @Override
                public String getNamespaceURI(String index) {
                    return "http://openstreetmap.org/osm/0.6";
                }

                @Override
                public String getNamespaceURI() {
                    return "http://openstreetmap.org/osm/0.6";
                }

                @Override
                public String getNamespacePrefix(int index) {
                    return "osm";
                }
            };
            var unmarshaller = context.createUnmarshaller();

            xmlReader.nextTag();
            xmlReader.nextTag();
            xmlReader.nextTag();

            Statement stmt = con.createStatement();

            HashSet<BigInteger> users = new HashSet<BigInteger>();
            var count = 0;

            while (wrapper.hasNext()) {
                if (cond.apply(count)) {
                    break;
                }
                JdbcImportMain.skipElements(wrapper);
                var element = unmarshaller.unmarshal(wrapper);
                if (!(element instanceof Node)) {
                    continue;
                }
                Node node = (Node) element;

                String userName = node.getUser();
                if (!users.contains(node.getUid())) {
                    String command = MessageFormat.format(osm_user_import, node.getUid(), userName.replace("'", "''"));
                    stmt.executeUpdate(command);
                    users.add(node.getUid());
                }
                var tags = new ArrayList<String>();
                if (node.getTag() != null) {
                    for (var t : node.getTag()) {
                        tags.add("('" + t.getK() + "','" + t.getV().replace("'", "") + "')::tag_pair");
                    }
                }
                String tagsAsTypes = tags.size() > 0 ? "ARRAY[" + String.join(",",tags) + "]" : "ARRAY[]::tag_pair[]";
                String sqlData = MessageFormat.format(node_import, node.getId(), node.getVersion(), node.getTimestamp(), node.getUid(), node.getChangeset(), node.getLat(), node.getLon(), tagsAsTypes);
                stmt.executeUpdate(sqlData);
                count++;
            }
            return count;
        } catch (XMLStreamException | SQLException | FileNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
