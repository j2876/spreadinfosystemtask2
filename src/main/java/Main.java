import org.apache.commons.cli.*;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openstreetmap.osm.Node;
import org.openstreetmap.osm.Osm;

import static javax.xml.stream.XMLStreamConstants.CHARACTERS;
import static javax.xml.stream.XMLStreamConstants.END_ELEMENT;

import javax.xml.bind.JAXBContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.*;
import java.util.*;

import static java.util.Comparator.comparing;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class.getName());

    static Set<Integer> types=new HashSet<>();
    static{
        types.add(CHARACTERS);
        types.add(END_ELEMENT);
    }
    static void skipElements(XMLStreamReader xmlReader) throws XMLStreamException {
        int eventType=xmlReader.getEventType();
        while(types.contains(eventType)){
            eventType=xmlReader.next();
        }
    }

    public static void main(String[] args) throws ParseException, IOException {
        int count = 0;
        try {
            JAXBContext context = JAXBContext.newInstance(Node.class);
            try (var in = new FileReader("test.xml")) {
                var xmlReader = XMLInputFactory.newInstance().createXMLStreamReader(in);
                XMLStreamReader wrapper = new StreamReaderDelegate(xmlReader) {
                    @Override
                    public int getNamespaceCount() {
                        return 1;
                    }

                    @Override
                    public String getNamespaceURI(int index) {
                        return "http://openstreetmap.org/osm/0.6";
                    }
                    @Override
                    public String getNamespaceURI(String index) {
                        return "http://openstreetmap.org/osm/0.6";
                    }
                    @Override
                    public String getNamespaceURI() {
                        return "http://openstreetmap.org/osm/0.6";
                    }

                    @Override
                    public String getNamespacePrefix(int index) {
                        return "osm";
                    }
                };
                var unmarshaller = context.createUnmarshaller();

                xmlReader.nextTag();
                xmlReader.nextTag();
                xmlReader.nextTag();

                while (wrapper.hasNext()) {
                    skipElements(wrapper);
                    var node = unmarshaller.unmarshal(wrapper);
                    count++;
                }
            }
        } catch (Exception exp) {
            exp.printStackTrace();
            System.out.println(count);
        }
    }
}
