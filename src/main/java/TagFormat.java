import org.openstreetmap.osm.Tag;

public class TagFormat {
    private final Tag tag;

    public TagFormat(Tag tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return String.format("('%s','%s')", tag.getK(), tag.getV().replace("\"", "").replace("'", "").replace(",","").replace("(","").replace(")",""));
    }
}