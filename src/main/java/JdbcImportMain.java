import org.apache.commons.cli.*;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openstreetmap.osm.Node;
import org.openstreetmap.osm.Osm;

import static javax.xml.stream.XMLStreamConstants.CHARACTERS;
import static javax.xml.stream.XMLStreamConstants.END_ELEMENT;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.*;
import java.math.BigInteger;
import java.sql.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Function;

import static java.util.Comparator.comparing;

public class JdbcImportMain {
    private static final Logger logger = LogManager.getLogger(Main.class.getName());

    static Set<Integer> types=new HashSet<>();
    static{
        types.add(CHARACTERS);
        types.add(END_ELEMENT);
    }
    static void skipElements(XMLStreamReader xmlReader) throws XMLStreamException {
        int eventType=xmlReader.getEventType();
        while(types.contains(eventType)){
            eventType=xmlReader.next();
        }
    }

    public static void main(String[] args) throws ParseException, IOException, JAXBException, XMLStreamException, SQLException {

        logger.log(Level.INFO, "Starting task 2");
        Options options = new Options();
        options.addOption("f", "file", true, "Input xml file");
        options.addOption("l", "limit", true, "Limit number of XML elements");
        options.addOption("u", "url", true, "JDBC url");
        options.addOption("t", "table", true, "node|node_json|node_ctype");
        options.addOption("m", "method", true, "insert | prepared_statement | batch");
        final var parser = new DefaultParser();
        var commandLine = parser.parse(options, args);

        int limit = Integer.parseInt(commandLine.getOptionValue('l', "1000"));
        var filePath = commandLine.getOptionValue('f');
        var url = commandLine.getOptionValue('u');
        var method = commandLine.getOptionValue('m');
        var table = commandLine.getOptionValue('t');

        String stmt_osm_user_import = "insert into osm_user values({0,number,#},''{1}'')";
        String stmt_node_import = "insert into node (id,version,cdate,uid, changeset,lat,lon) values({0,number,#},{1,number,#},''{2}'', {3,number,#}, {4,number,#}, {5,number,#}, {6,number,#})";
        String stmt_node_json_import = "insert into node_json (id,version,cdate,uid, changeset,lat,lon,tags) values({0,number,#},{1,number,#},''{2}'', {3,number,#}, {4,number,#}, {5,number,#}, {6,number,#}, ''{7}''::jsonb)";
        String stmt_tag_import = "insert into tag values({0,number,#},''{1}'',''{2}'')";

        String osm_user_import = "insert into osm_user values(?,?)";
        String node_import = "insert into node (id,version,cdate,uid, changeset,lat,lon) values(?,?,?,?,?,?,?)";
        String node_json_import = "insert into node_json (id,version,cdate,uid, changeset,lat,lon,tags) values(?,?,?,?,?,?,?,?::jsonb)";
        String tag_import = "insert into tag values(?,?,?)";

        String node_ctype_import = "insert into node_ctype (id,version,cdate,uid, changeset,lat,lon,tags_pairs) values({0,number,#},{1,number,#},''{2}'', {3,number,#}, {4,number,#}, {5,number,#}, {6,number,#}, {7})";
        String ps_node_ctype_import = "insert into node_ctype (id,version,cdate,uid, changeset,lat,lon,tags_pairs) values(?,?,?,?,?,?,?,?)";

        Map<String, DataLoader> loaders = new HashMap<>();
        loaders.put("node insert", new NodeStmtImport(stmt_osm_user_import, stmt_node_import, stmt_tag_import));
        loaders.put("node_json insert", new NodeStmtJsonImport(stmt_osm_user_import, stmt_node_json_import, stmt_tag_import));
        loaders.put("node_ctype insert", new NodeStmtCtypeImport(stmt_osm_user_import, node_ctype_import, stmt_tag_import));
        loaders.put("node prepared_statement", new NodePreparedStatementImport(osm_user_import, node_import, tag_import));
        loaders.put("node_json prepared_statement", new NodePreparedStatementJsonImport(osm_user_import, node_json_import, tag_import));
        loaders.put("node_ctype prepared_statement", new NodePreparedStatementCtypeImport(osm_user_import, ps_node_ctype_import, tag_import));
        loaders.put("node batch", new NodeBatchImport(osm_user_import, node_import, tag_import,200));
        loaders.put("node_json batch", new NodeBatchJsonImport(osm_user_import, node_json_import, tag_import,200));
        loaders.put("node_ctype batch", new NodeBatchCtypeImport(osm_user_import, ps_node_ctype_import, tag_import,200));

        var ourComb = table + " " + method;
        logger.log(Level.INFO, ourComb);
        var loader = loaders.get(ourComb);

        Connection con = DriverManager.getConnection(url);
        //con.setAutoCommit(false);
        loader.cleanup(con);
        var startTime = System.nanoTime();
        var count = loader.loadData(con, integer -> integer > limit);
        //con.commit();
        var elapsed = (System.nanoTime() - startTime) / 1e9d;
        logger.log(Level.INFO,  "Iterations count: " + count + " Time passed " + elapsed + " speed: " + count/elapsed);

        /*for (var loaderKey :
                loaders.keySet()) {
            loaders.get(loaderKey).cleanup(con);

            var startTime = System.nanoTime();
            var count = loader.loadData(con, new Function<Integer, Boolean>() {
                @Override
                public Boolean apply(Integer integer) {
                    return integer > limit;
                }
            });
            //con.commit();
            var elapsed = (System.nanoTime() - startTime) / 1e9d;
            logger.log(Level.INFO,  loaderKey+" Iterations count: " + count + " Time passed " + elapsed + " speed: " + count/elapsed);
        }*/
    }
}
